# Banco de Dados MySQL

## Como rodar
- Tenha certeza que tem `docker` e `docker-compose` instalados
- Na pasta `db/`, rodar `docker-compose up`
- O banco estará disponível em:

|Host|Porta|Usuário|Senha|
|--|--|--|--|
|localhost|3311|root|dwsenha|


create table dim_prova
(
    id     int auto_increment
        primary key,
    nu_ano int null
);

INSERT INTO ENADE.dim_prova (id, nu_ano) VALUES (1, 2017);
INSERT INTO ENADE.dim_prova (id, nu_ano) VALUES (2, 2017);
INSERT INTO ENADE.dim_prova (id, nu_ano) VALUES (3, 2018);
INSERT INTO ENADE.dim_prova (id, nu_ano) VALUES (4, 2019);
create table dim_sexo
(
    id      int auto_increment
        primary key,
    tp_sexo enum ('M', 'F') null
);

INSERT INTO ENADE.dim_sexo (id, tp_sexo) VALUES (1, 'F');
INSERT INTO ENADE.dim_sexo (id, tp_sexo) VALUES (2, 'M');
create table dim_presenca
(
    id          int auto_increment
        primary key,
    tp_presenca int          null,
    ds_presenca varchar(200) null
);

INSERT INTO ENADE.dim_presenca (id, tp_presenca, ds_presenca) VALUES (1, 555, 'Presente com resultado válido');
INSERT INTO ENADE.dim_presenca (id, tp_presenca, ds_presenca) VALUES (2, 222, 'Ausente');
INSERT INTO ENADE.dim_presenca (id, tp_presenca, ds_presenca) VALUES (3, 444, 'Ausente devido a dupla graduação');
INSERT INTO ENADE.dim_presenca (id, tp_presenca, ds_presenca) VALUES (5, 999, 'Presente por Ação judicial');
INSERT INTO ENADE.dim_presenca (id, tp_presenca, ds_presenca) VALUES (6, 334, 'Eliminado por participação indevida');
INSERT INTO ENADE.dim_presenca (id, tp_presenca, ds_presenca) VALUES (7, 888, 'Presente com resultado desconsiderado pelo Inep');
INSERT INTO ENADE.dim_presenca (id, tp_presenca, ds_presenca) VALUES (8, 556, 'Presente com resultado desconsiderado pela Aplicadora');
INSERT INTO ENADE.dim_presenca (id, tp_presenca, ds_presenca) VALUES (9, 333, 'Resultado desconsiderado por inscrição indevida');
create table dim_inscricao
(
    id               int auto_increment
        primary key,
    ds_inscricao     varchar(100) null,
    tp_inscricao_adm int          null
);

INSERT INTO ENADE.dim_inscricao (id, ds_inscricao, tp_inscricao_adm) VALUES (1, 'Tradicional', 0);
INSERT INTO ENADE.dim_inscricao (id, ds_inscricao, tp_inscricao_adm) VALUES (2, 'Judicial', 1);
INSERT INTO ENADE.dim_inscricao (id, ds_inscricao, tp_inscricao_adm) VALUES (3, 'Desconhecido', 2);
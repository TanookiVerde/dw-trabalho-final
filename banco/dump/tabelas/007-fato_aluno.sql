create table fato_aluno
(
	id int auto_increment
		primary key,
	fk_sexo int null,
	fk_curso int null,
	fk_prova int null,
	fk_universidade int null,
	fk_presenca int null,
	fk_turno int null,
	fk_inscricao int null,
	nu_idade int null,
	ano_fim_em int null,
	ano_in_grad int null,
	nt_fg decimal(3,1) null,
	nt_ger decimal(3,1) null,
	nt_dis_fg decimal(3,1) null,
	nt_obj_fg decimal(3,1) null,
	nt_ce decimal(3,1) null,
	constraint fato_aluno_dim_curso_id_fk
		foreign key (fk_curso) references dim_curso (id),
	constraint fato_aluno_dim_inscricao_id_fk
		foreign key (fk_inscricao) references dim_inscricao (id),
	constraint fato_aluno_dim_presenca_id_fk
		foreign key (fk_presenca) references dim_presenca (id),
	constraint fato_aluno_dim_prova_id_fk
		foreign key (fk_prova) references dim_prova (id),
	constraint fato_aluno_dim_sexo_id_fk
		foreign key (fk_sexo) references dim_sexo (id),
	constraint fato_aluno_dim_turno_id_fk
		foreign key (fk_turno) references dim_turno (id),
	constraint fato_aluno_dim_universidade_id_fk
		foreign key (fk_universidade) references dim_universidade (id)
);


create table dim_turno
(
    id       int auto_increment
        primary key,
    ds_turno varchar(100) null,
    co_turno int          null
);

INSERT INTO ENADE.dim_turno (id, ds_turno, co_turno) VALUES (1, 'Integral', 3);
INSERT INTO ENADE.dim_turno (id, ds_turno, co_turno) VALUES (2, 'Vespertino', 2);
INSERT INTO ENADE.dim_turno (id, ds_turno, co_turno) VALUES (3, 'Matutino', 1);
INSERT INTO ENADE.dim_turno (id, ds_turno, co_turno) VALUES (4, 'Noturno', 4);
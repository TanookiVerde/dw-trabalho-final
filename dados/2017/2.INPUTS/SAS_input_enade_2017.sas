/***************************************************************************** 
MEC/INEP/DAES (Diretoria de Avalia��o da Educa��o Superior) 
Coordena��o Geral de Controle de Qualidade da Educa��o Superior
------------------------------------------------------------------------------ 
Programa: 
	sas_input_enade_2017.sas (Pasta "INPUTS") 
------------------------------------------------------------------------------ 
Descri��o: 
	Programa para Leitura dos Microdados do enade 2017

****************************************************************************** 
Obs: Para executar este programa � necess�rio salvar o arquivo 
"microdados_enade_2017.txt" (Pasta "DADOS") no diret�rio "C:\" do computador.
 
******************************************************************************/
DATA WORK.microdados_enade_2017;
	 INFILE "C:\microdados_enade_2017.txt"
        LRECL=522
        ENCODING="LATIN1"
        DELIMITER=";"
		FIRSTOBS=2
        MISSOVER
        DSD;
   INPUT
        NU_ANO           : ?? BEST4.
        CO_IES           : ?? BEST5.
        CO_CATEGAD       : ?? BEST1.
        CO_ORGACAD       : ?? BEST5.
        CO_GRUPO         : ?? BEST4.
        CO_CURSO         : ?? BEST7.
        CO_MODALIDADE    : ?? BEST1.
        CO_MUNIC_CURSO   : ?? BEST7.
        CO_UF_CURSO      : ?? BEST2.
        CO_REGIAO_CURSO  : ?? BEST1.
        NU_IDADE         : ?? BEST2.
        TP_SEXO          : $CHAR1.
        ANO_FIM_EM       : ?? BEST4.
        ANO_IN_GRAD      : ?? BEST4.
        CO_TURNO_GRADUACAO : ?? BEST1.
        TP_INSCRICAO_ADM : ?? BEST1.
        TP_INSCRICAO     : ?? BEST1.
        NU_ITEM_OFG      : ?? BEST1.
        NU_ITEM_OFG_Z    : ?? BEST1.
        NU_ITEM_OFG_X    : ?? BEST1.
        NU_ITEM_OFG_N    : ?? BEST1.
        NU_ITEM_OCE      : ?? BEST2.
        NU_ITEM_OCE_Z    : ?? BEST1.
        NU_ITEM_OCE_X    : ?? BEST2.
        NU_ITEM_OCE_N    : ?? BEST1.
        DS_VT_GAB_OFG_ORIG : $CHAR8.
        DS_VT_GAB_OFG_FIN : $CHAR8.
        DS_VT_GAB_OCE_ORIG : $CHAR27.
        DS_VT_GAB_OCE_FIN : $CHAR27.
        DS_VT_ESC_OFG    : $CHAR8.
        DS_VT_ACE_OFG    : $CHAR8.
        DS_VT_ESC_OCE    : $CHAR27.
        DS_VT_ACE_OCE    : $CHAR27.
        TP_PRES          : ?? BEST3.
        TP_PR_GER        : ?? BEST3.
        TP_PR_OB_FG      : ?? BEST3.
        TP_PR_DI_FG      : ?? BEST3.
        TP_PR_OB_CE      : ?? BEST3.
        TP_PR_DI_CE      : ?? BEST3.
        TP_SFG_D1        : ?? BEST3.
        TP_SFG_D2        : ?? BEST3.
        TP_SCE_D1        : ?? BEST3.
        TP_SCE_D2        : ?? BEST3.
        TP_SCE_D3        : ?? BEST3.
        NT_GER           : ?? COMMAX4.
        NT_FG            : ?? COMMAX4.
        NT_OBJ_FG        : ?? COMMAX4.
        NT_DIS_FG        : ?? COMMAX4.
        NT_FG_D1         : ?? BEST3.
        NT_FG_D1_PT      : ?? BEST3.
        NT_FG_D1_CT      : ?? BEST3.
        NT_FG_D2         : ?? BEST3.
        NT_FG_D2_PT      : ?? BEST3.
        NT_FG_D2_CT      : ?? BEST3.
        NT_CE            : ?? COMMAX4.
        NT_OBJ_CE        : ?? COMMAX4.
        NT_DIS_CE        : ?? COMMAX4.
        NT_CE_D1         : ?? BEST3.
        NT_CE_D2         : ?? BEST3.
        NT_CE_D3         : ?? BEST3.
        CO_RS_I1         : $CHAR1.
        CO_RS_I2         : $CHAR1.
        CO_RS_I3         : $CHAR1.
        CO_RS_I4         : $CHAR1.
        CO_RS_I5         : $CHAR1.
        CO_RS_I6         : $CHAR1.
        CO_RS_I7         : $CHAR1.
        CO_RS_I8         : $CHAR1.
        CO_RS_I9         : $CHAR1.
        QE_I01           : $CHAR1.
        QE_I02           : $CHAR1.
        QE_I03           : $CHAR1.
        QE_I04           : $CHAR1.
        QE_I05           : $CHAR1.
        QE_I06           : $CHAR1.
        QE_I07           : $CHAR1.
        QE_I08           : $CHAR1.
        QE_I09           : $CHAR1.
        QE_I10           : $CHAR1.
        QE_I11           : $CHAR1.
        QE_I12           : $CHAR1.
        QE_I13           : $CHAR1.
        QE_I14           : $CHAR1.
        QE_I15           : $CHAR1.
        QE_I16           : ?? BEST2.
        QE_I17           : $CHAR1.
        QE_I18           : $CHAR1.
        QE_I19           : $CHAR1.
        QE_I20           : $CHAR1.
        QE_I21           : $CHAR1.
        QE_I22           : $CHAR1.
        QE_I23           : $CHAR1.
        QE_I24           : $CHAR1.
        QE_I25           : $CHAR1.
        QE_I26           : $CHAR1.
        QE_I27           : ?? BEST1.
        QE_I28           : ?? BEST1.
        QE_I29           : ?? BEST1.
        QE_I30           : ?? BEST1.
        QE_I31           : ?? BEST1.
        QE_I32           : ?? BEST1.
        QE_I33           : ?? BEST1.
        QE_I34           : ?? BEST1.
        QE_I35           : ?? BEST1.
        QE_I36           : ?? BEST1.
        QE_I37           : ?? BEST1.
        QE_I38           : ?? BEST1.
        QE_I39           : ?? BEST1.
        QE_I40           : ?? BEST1.
        QE_I41           : ?? BEST1.
        QE_I42           : ?? BEST1.
        QE_I43           : ?? BEST1.
        QE_I44           : ?? BEST1.
        QE_I45           : ?? BEST1.
        QE_I46           : ?? BEST1.
        QE_I47           : ?? BEST1.
        QE_I48           : ?? BEST1.
        QE_I49           : ?? BEST1.
        QE_I50           : ?? BEST1.
        QE_I51           : ?? BEST1.
        QE_I52           : ?? BEST1.
        QE_I53           : ?? BEST1.
        QE_I54           : ?? BEST1.
        QE_I55           : ?? BEST1.
        QE_I56           : ?? BEST1.
        QE_I57           : ?? BEST1.
        QE_I58           : ?? BEST1.
        QE_I59           : ?? BEST1.
        QE_I60           : ?? BEST1.
        QE_I61           : ?? BEST1.
        QE_I62           : ?? BEST1.
        QE_I63           : ?? BEST1.
        QE_I64           : ?? BEST1.
        QE_I65           : ?? BEST1.
        QE_I66           : ?? BEST1.
        QE_I67           : ?? BEST1.
        QE_I68           : ?? BEST1.
        QE_I69           : $CHAR1.
        QE_I70           : $CHAR1.
        QE_I71           : $CHAR1.
        QE_I72           : $CHAR1.
        QE_I73           : $CHAR1.
        QE_I74           : $CHAR1.
        QE_I75           : $CHAR1.
        QE_I76           : $CHAR1.
        QE_I77           : $CHAR1.
        QE_I78           : $CHAR1.
        QE_I79           : $CHAR1.
        QE_I80           : $CHAR1.
        QE_I81           : $CHAR1. 
		;
RUN;
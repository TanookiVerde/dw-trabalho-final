*************************************************************************************************
MEC/INEP/DAES (Diretoria de Avalia��o da Educa��o Superior)	
Coordena��o Geral de Controle de Qualidade da Educa��o Superior		
--------------------------------------------------------------------------------------------------------------------------
Programa:                                                                                         
	spss_input_enade_2017.sps (Pasta "INPUTS")                 	
---------------------------------------------------------------------------------------------------------------------------
Descri��o: 					
	Programa para Leitura dos Microdados do enade 2017            	
                                                                         		
***************************************************************************************************
 Obs: Para executar este programa � necess�rio salvar o arquivo          	
 "microdados_enade_2017.txt" (Pasta "DADOS") no diret�rio "C:\" do computador.	
						
***************************************************************************************************


GET DATA
/TYPE=TXT
/FILE="C:\microdados_enade_2017.txt"
/DELCASE=LINE
/DELIMITERS=";"
/QUALIFIER='"'
/ARRANGEMENT=DELIMITED
/FIRSTCASE=2
/IMPORTCASE=ALL
/VARIABLES =
        NU_ANO           		F4
        CO_IES           		F5
        CO_CATEGAD      	F5
        CO_ORGACAD      	F5
        CO_GRUPO         		F4
        CO_CURSO         		F7
        CO_MODALIDADE       	F1
        CO_MUNIC_CURSO      	F7
        CO_UF_CURSO      	F2
        CO_REGIAO_CURSO  	F1
        NU_IDADE         		F2
        TP_SEXO          		A1
        ANO_FIM_EM       	F4
        ANO_IN_GRAD         	F4
        CO_TURNO_GRADUACAO     	F1
        TP_INSCRICAO_ADM	F1
        TP_INSCRICAO        	F1
        NU_ITEM_OFG		        F1
        NU_ITEM_OFG_Z	    	F1
        NU_ITEM_OFG_X		F1
        NU_ITEM_OFG_N   	F1
        NU_ITEM_OCE         	F2
        NU_ITEM_OCE_Z       	F1
        NU_ITEM_OCE_X       	F2
        NU_ITEM_OCE_N       	F1
        DS_VT_GAB_OFG_ORIG        A8
        DS_VT_GAB_OFG_FIN           A8
        DS_VT_GAB_OCE_ORIG        A27
        DS_VT_GAB_OCE_FIN           A27
        DS_VT_ESC_OFG       	A8
        DS_VT_ACE_OFG       	A8
        DS_VT_ESC_OCE       	A27
        DS_VT_ACE_OCE       	A27
        TP_PRES          		F3
        TP_PR_GER        		F3
        TP_PR_OB_FG         	F3
        TP_PR_DI_FG      		F3
        TP_PR_OB_CE         	F3
        TP_PR_DI_CE      		F3
        TP_SFG_D1        		F3
        TP_SFG_D2        		F3
        TP_SCE_D1        		F3
        TP_SCE_D2        		F3
        TP_SCE_D3        		F3
        NT_GER                      	F4
        NT_FG            		F4
        NT_OBJ_FG        		F4
        NT_DIS_FG        		F4
        NT_FG_D1         		F3
        NT_FG_D1_PT      		F3
        NT_FG_D1_CT      		F3
        NT_FG_D2         		F3
        NT_FG_D2_PT      		F3
        NT_FG_D2_CT      		F3
        NT_CE            		F4
        NT_OBJ_CE        		F4
        NT_DIS_CE        		F4
        NT_CE_D1         		F3
        NT_CE_D2         		F3
        NT_CE_D3         		F3
        CO_RS_I1         		A1
        CO_RS_I2         		A1
        CO_RS_I3         		A1
        CO_RS_I4         		A1
        CO_RS_I5         		A1
        CO_RS_I6         		A1
        CO_RS_I7         		A1
        CO_RS_I8         		A1
        CO_RS_I9         		A1
        QE_I01           		A1
        QE_I02           		A1
        QE_I03           		A1
        QE_I04           		A1
        QE_I05           		A1
        QE_I06           		A1
        QE_I07           		A1
        QE_I08           		A1
        QE_I09           		A1
        QE_I10           		A1
        QE_I11           		A1
        QE_I12           		A1
        QE_I13           		A1
        QE_I14           		A1
        QE_I15           		A1
        QE_I16           		F2
        QE_I17           		A1
        QE_I18           		A1
        QE_I19           		A1
        QE_I20           		A1
        QE_I21           		A1
        QE_I22           		A1
        QE_I23           		A1
        QE_I24           		A1
        QE_I25           		A1
        QE_I26           		A1
        QE_I27           		F1
        QE_I28           		F1
        QE_I29           		F1
        QE_I30           		F1
        QE_I31           		F1
        QE_I32           		F1
        QE_I33           		F1
        QE_I34           		F1
        QE_I35           		F1
        QE_I36           		F1
        QE_I37           		F1
        QE_I38           		F1
        QE_I39           		F1
        QE_I40           		F1
        QE_I41           		F1
        QE_I42           		F1
        QE_I43           		F1
        QE_I44           		F1
        QE_I45           		F1
        QE_I46           		F1
        QE_I47           		F1
        QE_I48           		F1
        QE_I49           		F1
        QE_I50           		F1
        QE_I51           		F1
        QE_I52           		F1
        QE_I53           		F1
        QE_I54           		F1
        QE_I55           		F1
        QE_I56           		F1
        QE_I57           		F1
        QE_I58           		F1
        QE_I59           		F1
        QE_I60           		F1
        QE_I61           		F1
        QE_I62           		F1
        QE_I63           		F1
        QE_I64           		F1
        QE_I65           		F1
        QE_I66           		F1
        QE_I67           		F1
        QE_I68           		F1
        QE_I69                                   A1
        QE_I70                                   A1
        QE_I71                                   A1
        QE_I72                                   A1
        QE_I73                                   A1
        QE_I74                                   A1
        QE_I75                                   A1
        QE_I76                                   A1
        QE_I77                                   A1
        QE_I78                                   A1
        QE_I79                                   A1
        QE_I80                                   A1
        QE_I81                                   A1      
.
CACHE.
.
EXECUTE.


